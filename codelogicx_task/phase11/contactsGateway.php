<?php

class contactsGateway{
    public $con;
    public function __construct(){
        $this->con = mysqli_connect("localhost", "root", "123456", "test");
        
    }

    public function selectAll($order) {
        $sql = "SELECT * FROM contacts";
        $array = array();
        $query = mysqli_query($this->con,$sql);
        while($row = mysqli_fetch_assoc($query)){
            $array[] = $row;
        }
        return $array;
    }

    public function getContact($id){
        $sql = "SELECT * FROM contacts";
        $array = array();
        $query = mysqli_query($this->con,$sql);
        while($row = mysqli_fetch_assoc($query)){
            $array[] = $row;
        }
        return $array;
    }

    public function closeDB() {
        mysqli_close($this->con);
    }

    public function selectById($id) {
        
    }
}

$obj = new contactsGateway;

if(isset($_GET["selectAll"])){
    $id = $_GET["id"];
    $where = array("id"=>$id);
    if($obj->selectAll("contacts",$where)){
        header("location: contact-form.php");
    }
}

if(isset($_GET["getContact"])){
    $id = $_GET["id"];
    $where = array("id"=>$id);
    if($obj->getContact("contacts",$where)){
        header("location: contact-form.php");
    }
}

?>