<?php

require_once 'contactsGateway.php';

require_once 'database.php';

class contactService {
    private $contactsGateway   = NULL;

    private function openDb() {
        if (!mysqli_connect("localhost", "root", "123456")){
            throw new Exception("Connect to the database server failed!");
        }
        if (!mysqli_select_db($this->contactsGateway->con, "test")){
            throw new Exception("No test database found on database server.");
        }
    }
    
    private function closeDb(){
        $this->contactsGateway->closeDb();
    }

    public function __construct() {
        $this->contactsGateway = new contactsGateway();
    }

    public function getAllContacts($order){
        try {
            $this->openDb();
            $res = $this->contactsGateway->selectAll($order);
            $this->closeDb();
            return $res;
        } catch (Exeption $e){
            $this->closeDb();
            throw $e;
        }
    }

    public function getContact($id){
        try {
           $this->openDb();
           $res = $this->contactsGateway->selectById($id);
           $this->closeDb();
           return $res;
        } catch (Exeption $e){
          $this->closeDb();
          throw $e;
    } 
    return $this->contactsGateway->find($id);
    }

    private function validateContactParams( $name, $phone, $email, $address ) {
        $errors = array();
        if(  !isset($name) || empty($name)){
            $errors[] = 'Name is required';
        }
        if ( empty($errors) ){
            return;
        }
        throw new ValidationException($errors);
    }

    public function createNewContact( $name, $phone, $email, $address) {
        try {
            $this->openDb();
            $this->validateContactParams($name, $phone, $email, $address);
            $res = $this->contactsGateway->insert($name, $phone, $email, $address);
            $this->closeDb();
            return $res;
        }catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
    }

    public function deleteContact( $id ) {
        try{
            $this->openDb();
            $res = $this->contactsGateway->delete($id);
            $this->closeDb();
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
    }
}

$obj = new contactService;

if(isset($_GET["deleteContact"])){
    $id = $_GET["id"] ?? null;
    $where = array("id"=>$id);
    if($obj->delete_("contacts",$where)){
        header("location:contact-form.php");
    }
}


?>