<?php

class contactsController {

    private $contactService = NULL;

    public function __construct() {
        $this->contactService = new  contactService();
    }

    public function redirect($location) {
        header('Location: '.$location);
    }

    public function handleRequest() {
        $op = isset($_GET['op'])?$_GET['op']:NULL;
        try {
            if( !$op || $op == 'list')  {
                $this->listContacts();
            } elseif ( $op == 'new' ) {
                $this->saveContact();
            } elseif ( $op == 'delete' ) {
                $this->deleteContact();
            } elseif ( $op == 'show' ) {
                $this->showContact();
            } else {
                $this->showError("Page not found", "Page for operation ".$op." was not found!");
            }
        } catch ( Exception $e) {
            $this->showError("Aplication error", $e->getMessage());
        }
    }

    public function listContacts() {
        
        $orderby = isset($_GET['orderby'])?$_GET['orderby']:NULL;
        $contacts = $this->contactService->getAllContacts($orderby);
        echo ($contacts);
        include 'contacts.php';
    }

    public function saveContact() {
        $title = 'Add new contact';

        $name = '';
        $phone = '';
        $email = '';
        $address = '';

        $errors = array();

        if ( isset($_POST['form-submitted']) ){
            $name = isset($_POST['name']) ? $_POST['name'] :NULL;
            $phone = isset($_POST['phone']) ? $_POST['phone'] :NULL;
            $email = isset($_POST['email']) ? $_POST['email'] :NULL;
            $address = isset($_POST['address']) ? $_POST['address'] :NULL;

            try{
                $this->contactService->createNewContact($name, $phone, $email, $address);
                $this->redirect('index.php');
                return;
            } catch (ValidationException $e) {
                $errors = $e->getErrors();
            }
        }

        include 'contact-form.php';
    }

    public function deleteContact() {
        $id = isset($_GET['id'])?$_GET['id']:NULL;
        if ( !$id ) {
        throw new Exception('Internal error.');
        }
        $contact = $this->contactService->getContact($id);
        include 'contacts.php';
    } 

    public function showError($tittle, $message) {
        include 'error.php';
    }
}


$obj = new contactsController;

if(isset($_GET["listContacts"])){
    $id = $_GET["id"];
    $where = array("id"=>$id);
        if($obj->listContacts("contacts",$where)){
            header("location: contact-form.php");
        }
    }

if(isset($_POST["saveContact"])){
  $myArray = array (
      "name" => $_POST["name"],
      "phone" => $_POST["phone"],
      "email" => $_POST["email"],
      "address" => $_POST["address"],
  );

  if($obj->saveContact("contacts",$myArray)){
      header("location: contact-form.php");
  }
}

if(isset($_GET["deleteContact"])){
    $id = $_GET["id"] ?? null;
    $where = array("id"=>$id);
    if($obj->deleteContact("contacts",$where)){
        header("location: contact-form.php");
    }
}

if(isset($_GET["showError"])){
    $id = $_GET["id"] ?? null;
    $where = array("id"=>$id);
    if($obj->showError("contacts",$where)){
        header("location: contact-form.php");
    }
}

?>