<?php

include_once "database.php";

class PhpOperation extends Database
{
    public function insert_record($table,$fields){
        //"INSERT INTO table_name (, , , , ,) VALUES ('name','phone','email','address','id')";
        $sql = "";
        $sql .= "INSERT INTO ".$table;
        $sql .= " (".implode(",", array_keys($fields)).") VALUES";

        $sql .= "('".implode("','", array_values($fields))."')";

        echo($sql);
        
        $query = mysqli_query($this->con, $sql);
        if($query) {
            return true;
        }
 
    }
    public function fetch_record($table){
        $sql = "SELECT * FROM " .$table;
        $array = array();
        $query = mysqli_query($this->con,$sql);
        while($row = mysqli_fetch_assoc($query)){
            $array[] = $row;
        }
        return $array;
    }
    public function select_record($table,$where){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value){
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        $sql .= "SELECT * FROM ".$table." WHERE ".$condition;
        $query = mysqli_query($this->con,$sql);
        $row = mysqli_fetch_array($query);
        return $row;
    }
    public function update_record($table,$where,$fields){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value){
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        foreach ($fields as $key => $value){
            $sql .= $key . "='".$value."', ";
        }
        $sql = substr($sql, 0,-2);
        $sql = "UPDATE ".$table." SET ".$sql." WHERE ".$condition;
        if(mysqli_query($this->con,$sql)){
            return true;
        }
    }
    public function delete_record($table,$where){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value){
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        $sql = "DELETE FROM ".$table." WHERE ".$condition;
        if(mysqli_query($this->con,$sql)){
            return true;
        }
    }
    
}


$obj = new PhpOperation;

print_r($_POST);

if(isset($_POST["save"])){

    echo("here");

    $myArray = array(
        "name" => $_POST["name"],
        "phone" => $_POST["phone"],
        "email" => $_POST["email"],
        "address" => $_POST["address"],
    );
    
    if($obj->insert_record("contacts",$myArray)){
        echo("done");
        header("location: contact-form.php");
    } 
}

if(isset($_POST["edit"])){
    $product_id = $_POST["id"];
    $where = array("id"=>$id);
    $myArray = array(
        "name" => $_POST["name"],
        "phone" => $_POST["phone"],
        "email" => $_POST["email"],
        "address" => $_POST["address"],
        );
        if($obj->update_record("contacts",$where,$myArray)){
            header("location:contact-form.php");
        }
}

if(isset($_GET["delete"])){
    $id = $_GET["id"] ?? null;
    $where = array("id"=>$id);
    if($obj->delete_record("contacts",$where)){
        header("location:contact-form.php");
    }
}

?>