<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contacts</title>
    
    <style type="css">
    table.contacts {
        width: 100%;
    }

    table.contacts thead {
        background-color: #eee;
        text-align: left;
    }

    table.contacts thead th {
        border: solid 1px #fff;
        padding: 3px;
    }

    table.contacts tbody td {
        border: solid 1px #eee;
        padding: 3px;
    }

    a, a:hover, a:active, a:visited {
        color: blue;
        text-decoration: underline;
    }
    </style>
</head>
<body>
    <div><a href="index.php?op=new">Add new contact</a>
</div>
<table border="0" cellpadding="0" cellspacing="0">
 <thead>
   <tr>
     <th><a href="?orderby=name">Name</a></th>
     <th><a href="?orderby=phone">Phone</a></th>
     <th><a href="?orderby=email">Email</a></th>
     <th><a href="?orderby=address">Address</a></th>
     <th>&nbsp;</th>
   </tr>
 </thead>
 <tbody>
 <?php foreach ($contacts as $contact): ?>
<tr>
<td><a href="index.php?op=show&id=<?php echo $contact["id"] ?>">
<?php echo $contact["name"]; ?></a></td>
<td><?php echo $contact["phone"] ?></td>
<td><?php echo $contact["email"] ?></td>
<td><?php echo $contact["address"] ?></td>
<td><a href="index.php?op=delete&id=<?php echo $contact["id"] ?>">
delete</a></td>
     </tr>   
     <?php endforeach; ?>
</tbody>
</table>
</body>
</html>