<?php

 include "action.php";

session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP CRUD OOP</title>
    
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="libs/css/custom.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <style>
    .m-r-1em{ margin-right:1em; }
    .m-b-1em{ margin-bottom:1em; }
    .m-l-1em{ margin-left:1em; }
    .mt0{ margin-top:0; }
    </style>
</head>

<body>

   <div classs="container">
     <div class="jumbotron">
       <h1>Products <small>Choose yours</small></h2>
     </div>
   </div>
   <div class="container">
     <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
           <div class="panel panel-primary">
              <div class="panel-heading">Enter products details</div>
              <div class="panel-body"></div>
                  <?php
                
                    if(isset($_GET["update"])){
                      //php 7
                      $product_id = $_GET["product_id"] ?? null ;
                      $where = array("product_id"=>$product_id);
                      $row = $obj->select_record("product_s",$where);
                    ?>
                    
                      <form action="action.php" method="POST">
                        <table class="table table-hover">
                        <tr>
                             <td><input type="hidden" name="product_id" value="<?php echo $product_id; ?>"></input></td>
                          </tr>
                          <tr>
                          <tr>
                             <td>Product Name</td>
                             <td><input type="text" name="name" class="form-control" value="<?php echo $row["product_name"]; ?>" placeholder="Enter the product name"></input></td>
                          </tr>
                          <tr>
                              <td>Description</td>
                              <td><input type="description" name="description" class="form-control" value="<?php echo $row["description"]; ?>" placeholder="Enter the description"></input></td>
                          </tr>
                          <tr>
                              <td>Price</td>
                              <td><input type="text" name="price" class="form-control" value="<?php echo $row["price"]; ?>" placeholder="Enter the price"></input></td>
                          </tr>
                          <tr>   
                              <td colspan="2" align="left">
                              <button type="submit" name="edit" 
                              class="btn btn-primary" value="update">Save</button>
                          </tr>
                        </table>
                      </form>
                   
                    <?php
                    }else{
                      ?>
                  
                <form action="action.php" method="POST">
                  <table class="table table-hover">
                    <tr>
                      <td>Product Name</td>
                      <td><input type="text" name="name" class="form-control" placeholder="Enter the product name"></input></td>
                    </tr>
                    <tr>
                      <td>Description</td>
                      <td><input type="description" name="description" class="form-control" placeholder="Enter the description"></input></td>
                    </tr>
                    <tr>
                      <td>Price</td>
                      <td><input type="number" name="price" class="form-control" placeholder="Enter the price"></input></td>
                    </tr>
                    <tr>
                       <td>Choose the categories</td>
                       <td>
                       <select name="id" id="categories">
                        <option disabled>Select one</option>
                        <?php
                          $myrow = $obj->fetch_record("categories");
                          foreach ($myrow as $row){
                            ?>
                              <option value="<?php echo $row["id"] ?>">
                                <?php echo $row["name"] ?>
                              </option>
                            <?php
                          }
                          ?>
                       </select>
                       </td>
                    </tr>
                    <tr>
                      <td colspan="2" align="left">
                      <button type="submit" name="save" 
                        class="btn btn-primary" value="save">Save</button>
                    </tr>
                  </table>
                </form>

            
           
                <?php
                    }
                
                ?>
             

           </div>
        </div>
        <div class="col-md-3"></div>
     </div>
   </div>

   <div class="container">
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
         <table class="table table-borderrd">
          <tr>
            <th>#</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
          </tr>
          <?php
            $myrow = $obj->fetch_record("product_s");
            foreach ($myrow as $row){
              //breaking point
              ?>
                <tr>
                  <td><?php echo $row["product_id"]; ?></td>
                  <td><?php echo $row["product_name"]; ?></td>
                  <td><?php echo $row["description"]; ?></td>
                  <td><?php echo $row["price"]; ?></td>
                  <td><a href="php_crud_oop.php?update=1&product_id=<?php echo $row["product_id"]; ?>" 
                         class="btn btn-info">Edit</a></td>
                  <td><a href="action.php?delete=1&product_id=<?php echo $row["product_id"]; ?>" 
                         class="btn btn-danger">Delete</a></td>
                  <td><a href="read.php?read=1&product_id=<?php echo $row["product_id"]; ?>" 
                         class="btn btn-info">Read</a></td>
                </tr>

              <?php
        
            }
      
              ?>
         </table> 
         <div class="page-header">
             <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>.
         </div>
         <p>
           <a href="reset-password.php" class="btn btn-warning">Reset Your Password</a>
           <a href="logout.php" class="btn btn-danger">Sign Out of Your Account</a>
         </p>
      </div>
    </div>
   </div>

</body>
</html>