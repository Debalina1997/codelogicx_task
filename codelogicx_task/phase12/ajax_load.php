<?php

$conn = mysqli_connect("localhost","root","123456","test") or die("connection Failed");

$sql = "SELECT * FROM user_data";
$result = mysqli_query($conn, $sql) or die("SQL Query Failed.");
$output = "";
if(mysqli_num_rows($result) > 0){
    $output = '<table border="2" width="100%" cellspacing="0" cellpadding="10px">
    <tr>
      <th width="100px">ID</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Email</th>
      <th>Message</th>
      <th width="100px">Delete</th>
      <th width="100px">View</th>
    </tr>';

    while($row = mysqli_fetch_assoc($result)){
        $output .= "<tr><td>{$row["id"]}</td>
        <td>{$row["firstName"]}</td>
        <td>{$row["lastName"]}</td>
        <td>{$row["email"]}</td>
        <td>{$row["message"]}</td>
        <td><button class='delete-btn' data-id='{$row["id"]}'>Delete</button></td>
        <td><button class='info-btn' data-id='{$row["id"]}'>View</button></td>
        </tr>";
    }
    $output .= "</table>";

    mysqli_close($conn);
    echo $output;

}else{
    echo "<h2>No Record Found.</h2>";
}

?>