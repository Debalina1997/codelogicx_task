<?php
  include_once "ajax_load.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajax POST request with jQuery and PHP</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <style type="text/css">

    body {
       font-family: calibri;
    }
    .box {
        margin-bottom: 10px;
    }
    .box label {
        display: inline-block;
        width: 80px;
        text-align: right;
        margin-right: 10px;
    }
    .box input, .box textarea {
        border-radius: 3px;
        border: 1px solid #ddd;
        padding: 5px 10px;
    }
    .btn-submit {
        margin-left: 90px;
    }
    .delete-btn{
        background: red;
        color: #fff;
        border:0;
        padding: 4px 10px;
        border-redius: 3px;
        cursor: pointer;
    }
    .info-btn{
        background: blue;
        color: #fff;
        border:0;
        padding: 4px 10px;
        border-redius: 3px;
        cursor: pointer;
    }
  </style>
</head>
<body>
    
    <h2>Ajax POST request with JQuery and PHP</h2>
    <form>
        <div class="box">
            <label>First Name:</label><input type="text" name="firstName" id="firstName" />
        </div>
        <div class="box">
            <label>Last Name:</label><input type="text" name="lastName" id="lastName" />
            </div>
            <div class="box">
            <label>Email:</label><input type="email" name="email" id="email" />
            </div>
            <div class="box">
            <label>Message:</label><textarea type="text" name="message" id="message"></textarea>
            </div>
            <input id="submit" type="button" class="btn-submit" value="Submit" />
    </form>
    
    <script>
      $(document).ready(function() {
         $("#submit").click(function(){
             var firstName = $("#firstName").val();
             var lastName = $("#lastName").val();
             var email = $("#email").val();
             var message = $("#message").val();

             if(firstName==''||lastName==''||email==''||message=='') {
             alert("Please fill all fields.");
             return false;
             }

             $.ajax({
                 type: "POST",
                 url: "submission.php",
                 data: {
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    message: message
                 },
                 cache: false,
                 success: function(data) {
                 alert(data);
                 },
                 error: function(xhr, status, error) {
                 console.error(xhr);
                 }
             });
        });
        $(document).on("click",".delete-btn", function(){
            if(confirm("Do you really want to delete this record?")){

               var id = $(this).data("id");
               var element = this; 
             
               $.ajax({
                   type: "POST",
                   url: "delete.php",
                   data: {
                      id: id 
                   },
                   cache: false,
                   success: function(data){
                       if(data == 1){
                          $(element).closest("tr").fadeout();
                       }else{
                           $("#error-message").html( "cant't Delete Record").slidedown();
                           $("#success-message").slideup();
                       }
                   }
               });
            }
        });
        $(document).on("click",".info-btn", function(){
            if(confirm("Do you want view the data")){
                var response = '';

                var id = $(this).data("id");

                 $.ajax({
                    type: "POST",
                    url: "view.php",
                    data: {
                       id: id
                    //    firstName: firstName,
                    //    lastName: lastName,
                    //    email: email,
                    //    message: message
                    },
                   cache: false,
                   success: function(data) {
                    //    console.log(data);
                       response = data;
                       alert(response);
                   },
                   error: function(xhr, status, error) {
                        console.error(xhr);
                   }
                });
               
            }
        })
    });
    </script>

</body>
</html>