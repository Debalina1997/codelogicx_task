<?php

session_start();

$mysqli = new mysqli('localhost', 'root', 123456, 'test') or die(mysqli_error($mysqli));

$update = false;
$name='';
$description='';
$price='';

if (isset($_POST['save'])){

  $product_id = $_POST['id'];

  $product_name = $_POST['name'];
  $description = $_POST['description'];
  $price = $_POST['price'];

  echo($product_name);
  echo($description);
  echo($price);

  $mysqli->query("INSERT INTO products(`product_name`, `description`, `price`) VALUES('$product_name', '$description', $price)") or
           die($mysqli->error);

  $_SESSION['message'] = "Record has been saved!";
  $_SESSION['msg_type'] = "success";

  header("location: php_crud.php");
}

if (isset($_GET['delete'])){
  $product_id = $_GET['delete'];
  $mysqli->query("DELETE FROM products WHERE product_id=$product_id") or die($mysqli->error);

  $_SESSION['message'] = "Record has been deleted!";
  $_SESSION['msg_type'] = "danger";

  header("location: php_crud.php");
}

// if (isset($_GET['edit'])){
//   $product_id = $_GET['edit'];
//   $update = true;
//   $result = $mysqli->query("SELECT * FROM products WHERE product_id=$product_id") or die($mysqli->error);
//   if (count($result)==1){
//     $row = $result->fetch_array();
//     $name = $row['product_name'];
//     $description = $row['description'];
//     $price = $row['price'];
//   }
//   header("location: product.php");
// }

// product.php => process.php => php_crud.php

if(isset($_POST['update'])){
  $product_id = $_POST['product_id'];
  $name = $_POST['name'];
  $description = $_POST['description'];
  $price = $_POST['price'];

  $mysqli->query("UPDATE products SET product_name='$name', description='$description', price=$price WHERE product_id=$product_id") or
           die($mysqli->error);
           
  $_SESSION['message'] = "Record has been updated!";
  $_SESSION['msg_type'] = "Warning";

  header('location: php_crud.php');
}