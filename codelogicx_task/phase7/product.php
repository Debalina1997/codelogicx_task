<?php

$name='';
$description='';
$price='';

session_start();

$mysqli = new mysqli('localhost', 'root', 123456, 'test') or die(mysqli_error($mysqli));


if (isset($_GET['id'])) {

    $product_id = $_GET['id'];
    $result = $mysqli->query("SELECT * FROM products WHERE product_id=$product_id") or die($mysqli->error);

    if ($result->num_rows == 1) {
        while ($row = $result->fetch_assoc()) {
            $name = $row['product_name'];
            $description = $row['description'];
            $price = $row['price'];
        }
    }
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <form action="process.php" method="POST">
                    <input type="hidden" name="product_id" value='<?php echo $product_id ?>' />
                    <div class="form-group">
                        <input type="text" class="form-control" value='<?php echo $name ?>' name="name" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" value='<?php echo $description ?>' name="description" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" value='<?php echo $price ?>' name="price" />
                    </div>
                    <button name="update" class="btn btn-info">Edit</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
