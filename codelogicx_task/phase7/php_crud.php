<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Crud</title>
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>

    <style>
    .m-r-1em{ margin-right:1em; }
    .m-b-1em{ margin-bottom:1em; }
    .m-l-1em{ margin-left:1em; }
    .mt0{ margin-top:0; }
    </style>

</head>

<body>
    <?php require_once 'process.php'; ?>

    <?php
      if (isset($_SESSION['message'])): ?>

      <div class='alert alert-<?=$_SESSION['msg_type']?>'>

       <?php 
         echo $_SESSION['message'];
         unset($_SESSION['message']);
       ?>
      </div>
      <?php endif ?>
    <div class="container">
    <?php
      $mysqli = new mysqli('localhost', 'root', 123456, 'test') or die(mysqli_error($mysqli));
      $result = $mysqli->query("select * from products") or die($mysqli->error);
      //pre_r($result);
    ?>
    <div class="row justify-content-left">
      <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            </th colspan="2">Action</th>
          </tr>
        </thead>

    <?php
      while ($row = $result->fetch_assoc()):?>
        <tr>
          <td>
            <a href="product.php?id=<?php echo $row['product_id']; ?>">
              <?php echo $row['product_name']; ?>
            </a>
          </td>
          <td><?php echo $row['description']; ?></td>
          <td><?php echo $row['price']; ?></td>
          <td>
            <a href="process.php?edit=<?php echo $row['product_id']; ?>"
              class="btn btn-info">Edit</a>
            <a href="process.php?delete=<?php echo $row['product_id']; ?>"
              class="btn btn-danger">Delete</a>
          </td>
        </tr>
      <?php endwhile; ?>  
      </table>
    </div>
    <?php
      
      function pre_r( $array ) {
          echo '<pre>';
          print_r($array);
          echo '</pre>';
        }
      ?>
    <div class="row justify-content-left">
        <form action="process.php" method="POST">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control" 
                placeholder="Enter product name" />
            </div>
            <div class="form-group">
                <label>Description</label>
                <input type="description" name="description" class="form-control" 
                placeholder="Enter the description" />
            </div>
            <div class="form-group">
                <label>Price</label>
                <input type="text" name="price" class="form-control" 
                placeholder="Entter the price" />
            </div>
            <div class="form-group">
                <?php
                if ($update == true):
                ?>
                   <button type="submit" name="update" class="btn btn-info">Update</button>
                <?php else: ?>
                   <button type="submit" name="save" class="btn btn-primary">Save</button>
                <?php endif ?>
            </div>
            <div class="form-group">
                <a href="php_crud.html" class="btn btn-danger">Back to read products</a>
            </div>
        </form>
    </div>
    </div>
</body>

</html>