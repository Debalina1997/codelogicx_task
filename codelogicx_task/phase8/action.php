<?php

include_once "databse.php";

class DataOperation extends Database
{
    public function insert_record($table,$fields){
        //"INSERT INTO table_name (, , , , ) VALUES ('product_name','description','price','id')";
        $sql = "";
        $sql .= "INSERT INTO ".$table;
        $sql .= " (".implode(",", array_keys($fields)).") VALUES";

        // $sql .= "(";
        // foreach ($fields as $key => $value){
        //     //id = '5' AND product_name = 'something'
        //     if ($key == "price") {
        //         $sql .= $value;
        //     } else {
        //         $sql .= "'".$value . "',";
        //     }
        // }
        // $sql .= ");";

        // echo ($sql);

        $sql .= "('".implode("','", array_values($fields))."')";
        
        $query = mysqli_query($this->con, $sql);
        if($query) {
            return true;
        }
 
    }
    public function fetch_record($table){
        $sql = "SELECT * FROM " .$table;
        $array = array();
        $query = mysqli_query($this->con,$sql);
        while($row = mysqli_fetch_assoc($query)){
            $array[] = $row;
        }
        return $array;
    }
    public function select_record($table,$where){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value){
            //id = '5' AND product_name = 'something'
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        $sql .= "SELECT * FROM ".$table." WHERE ".$condition;
        $query = mysqli_query($this->con,$sql);
        $row = mysqli_fetch_array($query);
        return $row;
    }
    public function update_record($table,$where,$fields){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value){
            //id = '5' AND product_name = 'something'
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        foreach ($fields as $key => $value){
            //UPDATE table SET product_name = '' , description = '' WHERE product_id = '';
            $sql .= $key . "='".$value."', ";
        }
        $sql = substr($sql, 0,-2);
        $sql = "UPDATE ".$table." SET ".$sql." WHERE ".$condition;
        if(mysqli_query($this->con,$sql)){
            return true;
        }
    }
    public function delete_record($table,$where){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value){
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        $sql = "DELETE FROM ".$table." WHERE ".$condition;
        if(mysqli_query($this->con,$sql)){
            return true;
        }
    }
    
}


$obj = new DataOperation;

if(isset($_POST["save"])){

    $myArray = array(
        "product_name" => $_POST["name"],
        "description" => $_POST["description"],
        "price" => $_POST["price"],
        "id"=>$_POST["id"],
    );
    
    if($obj->insert_record("product_s",$myArray)){
        echo("done");
        header("location: php_crud_oop.php");
    } 
}

if(isset($_POST["edit"])){
    $product_id = $_POST["product_id"];
    $where = array("product_id"=>$product_id);
    $myArray = array(
        "product_name" => $_POST["name"],
        "description" => $_POST["description"],
        "price" => $_POST["price"],
        );
        if($obj->update_record("product_s",$where,$myArray)){
            header("location:php_crud_oop.php?msg=Record updated Successfully");
        }
}

if(isset($_GET["delete"])){
    $product_id = $_GET["product_id"] ?? null;
    $where = array("product_id"=>$product_id);
    if($obj->delete_record("product_s",$where)){
        header("location:php_crud_oop.php?msg=Record Deleted Successfully");
    }
}

?>