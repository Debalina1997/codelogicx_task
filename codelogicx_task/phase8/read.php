<?php

include_once "databse.php";

class ReadOperation extends Database
{
    public function insert_record($table,$fields){
        //"INSERT INTO table_name (, , , , ) VALUES ('product_name','description','price','id')";
        $sql = "";
        $sql .= "INSERT INTO ".$table;
        $sql .= " (".implode(",", array_keys($fields)).") VALUES";

        // $sql .= "(";
        // foreach ($fields as $key => $value){
        //     //id = '5' AND product_name = 'something'
        //     if ($key == "price") {
        //         $sql .= $value;
        //     } else {
        //         $sql .= "'".$value . "',";
        //     }
        // }
        // $sql .= ");";

        // echo ($sql);

        $sql .= "('".implode("','", array_values($fields))."')";
        
        $query = mysqli_query($this->con, $sql);
        if($query) {
            return true;
        }
 
    }
     
    public function fetch_single($table,$where){
      $sql = "";
      $condition = "";
      foreach ($where as $key => $value){
      $condition .=$key . "='" . $value . "' AND ";
      }
      $condition = substr($condition, 0, -5);
      $sql = substr($sql, 0,-2);
      $sql = "SELECT ".$table." SET ".$sql." WHERE ".$condition;
      if(mysqli_query($this->con,$sql)){
          return true;
      }
  }
}

$obj = new ReadOperation;

if(isset($_GET["read"])){
  $product_id = $_GET["product_id"];
  $where = array("product_id"=>$product_id);
      if($obj->fetch_single("product_s",$where)){
          header("location:read.php?msg=Record Read Successfully");
      }
}

  ?>

<?php
  include "action.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP CRUD OOP</title>
    
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="libs/css/custom.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <style>
    .m-r-1em{ margin-right:1em; }
    .m-b-1em{ margin-bottom:1em; }
    .m-l-1em{ margin-left:1em; }
    .mt0{ margin-top:0; }
    </style>


</head>

<body>
   <div class="container">
     <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
           <div class="panel panel-primary">
              <div class="panel-heading">Products details</div>
              <div class="panel-body"></div>
                  <?php
                
                    if(isset($_GET["read"])){
                      //php 7
                      $product_id = $_GET["product_id"] ?? null ;
                      $where = array("product_id"=>$product_id);
                      $row = $obj->select_record("product_s",$where);
                    ?>
                    
                      <form action="action.php" method="POST">
                        <table class="table table-hover">
                        <tr>
                             <td><input type="hidden" name="product_id" value="<?php echo $product_id; ?>"></input></td>
                          </tr>
                          <tr>
                          <tr>
                             <td>Product Name</td>
                             <td><input type="text" name="name" class="form-control" value="<?php echo $row["product_name"]; ?>" placeholder="Enter the product name"></input></td>
                          </tr>
                          <tr>
                              <td>Description</td>
                              <td><input type="description" name="description" class="form-control" value="<?php echo $row["description"]; ?>" placeholder="Enter the description"></input></td>
                          </tr>
                          <tr>
                              <td>Price</td>
                              <td><input type="text" name="price" class="form-control" value="<?php echo $row["price"]; ?>" placeholder="Enter the price"></input></td>
                          </tr>
                          <?php
                             $myrow = $obj->fetch_record("product_s");
                             foreach ($myrow as $row){
                          ?>
                          
                          <?php
                             }
                          ?>
                        </table>
                      </form>
                      <?php
        
                        }
      
                      ?>
            </div>
        </div>
        <div class="col-md-3"></div>
     </div>
   </div>

   </body>
   </html>