<section>
 <div class="container">
   <article class="blog-post">
     <h1><?= $post['title'] ?></h1>
     <div class="details">
       posted on: <?= date('M D Y', strtotime($post['create_date'])) ?> by <a href="https://www.alexlancer.com">Alex Lancer</a>
     </div>
     <?= $post['body'] ?>
   </article>
 </div>
</section>