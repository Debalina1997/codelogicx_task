<?php

if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", 
        "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo"]["name"];
        $filetype = $_FILES["photo"]["type"];
        $filesize = $_FILES["photo"]["size"];

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: 
            Please select a valid file format.");

            $maxsize = 5 * 1024 * 1024;
            if($filesize > $maxsize) die("Error: File size is larger than allowed limit.");
            
            if(in_array($filetype, $allowed)){
                
                if(file_exists("upload/" . $filename)){
                    echo $filename . " is already exists.";
                }else{
                    print_r($filename);
                    print_r($_FILES["photo"]["tmp_name"]);
                    move_uploaded_file($_FILES["photo"]["tmp_name"], "upload/" . $filename);
                    echo "your file is uploaded successfully";
                }
            }else{
                echo "Error: There is a problem uploading your file. 
                Please try again";
            }
    }else{
        echo "Error: " . $_FILES["photo"]["error"];
    }
}

if($_FILES["photo"]["error"] > 0){
    echo "Error: " . $_FILES["photo"]["error"] . "<br>";
} else{
    echo "File Name: " . $_FILES["photo"]["name"] . "<br>";
    echo "File Type: " . $_FILES["photo"]["type"] . "<br>";
    echo "File Size: " . ($_FILES["photo"]["size"] / 1024) . " KB<br>";
    echo "Stored in: " . $_FILES["photo"]["tmp_name"];
}


?>