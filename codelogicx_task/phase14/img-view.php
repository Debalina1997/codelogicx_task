<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Display</title>

    <link type="text/css" rel="stylesheet" href="./css/lightslider.css" />

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="./js/lightslider.js"></script>


    
</head>

<body>
    <ul id="lightslider">
        <li class="item-a" data-thumb="http://sachinchoolur.github.io/lightslider/picture/thumb/download.jpg">
                    <img src="picture/download.jpg" alt="">
        </li>
        </ul>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        $('#lightslider').lightSlider({
            // item: 1,
            // autoWidth: false,
            // slideMove: 1, // slidemove will be 1 if loop is true
            // slideMargin: 10,

            // addClass: '',
            // mode: "slide",
            // useCSS: true,
            // cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            // easing: 'linear', //'for jquery animation',////

            // speed: 400, //ms'
            // auto: false,
            // loop: false,
            // slideEndAnimation: true,
            // pause: 2000,

            // keyPress: false,
            // controls: true,
            // prevHtml: '',
            // nextHtml: '',

            // rtl: false,
            // adaptiveHeight: false,

            // vertical: false,
            // verticalHeight: 500,

            // vThumbWidth: 100,

            // thumbItem: 10,
            // pager: true,
            // gallery: false,
            // galleryMargin: 5,
            // thumbMargin: 5,
            // currentPagerPosition: 'middle',

            // enableTouch: true,
            // enableDrag: true,
            // freeMove: true,
            // swipeThreshold: 40,

            // responsive: [],

            // onBeforeStart: function (el) { },
            // onSliderLoad: function (el) { },
            // onBeforeSlide: function (el) { },
            // onAfterSlide: function (el) { },
            // onBeforeNextSlide: function (el) { },
            // onBeforePrevSlide: function (el) { }
        });
    });
</script>

</html>