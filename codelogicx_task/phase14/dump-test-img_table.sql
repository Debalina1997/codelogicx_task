-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `img`
--

DROP TABLE IF EXISTS `img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `img` (
  `img_id` int NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) DEFAULT NULL,
  `img_dir` varchar(255) DEFAULT NULL,
  `img_type` varchar(50) DEFAULT NULL,
  `img_size` varchar(50) DEFAULT NULL,
  `id` int DEFAULT NULL,
  PRIMARY KEY (`img_id`),
  KEY `id` (`id`),
  CONSTRAINT `img_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `img`
--

LOCK TABLES `img` WRITE;
/*!40000 ALTER TABLE `img` DISABLE KEYS */;
INSERT INTO `img` VALUES (1,'bird2.jpg','upload/deba11@gmail.com/bird2.jpg','image/jpeg','5819',4),(2,'bird3.jpg','upload/deba11@gmail.com/bird3.jpg','image/jpeg','7195',4),(3,'bird5.jpg','upload/deba11@gmail.com/bird5.jpg','image/jpeg','3835',4),(4,'bird8.jpg','upload/deba11@gmail.com/bird8.jpg','image/jpeg','7746',4),(5,'bird4.jpg','upload/deba11@gmail.com/bird4.jpg','image/jpeg','16466',4),(6,'bird7.jpg','upload/deba11@gmail.com/bird7.jpg','image/jpeg','9106',4),(7,'bird5.jpg','upload/deba11@gmail.com/bird5.jpg','image/jpeg','3835',4),(8,'download.jpg','upload/mc@gmail.com/download.jpg','image/jpeg','6017',8),(9,'flower.jpg','upload/mc@gmail.com/flower.jpg','image/jpeg','7721',8),(10,'flower1.jpg','upload/mc@gmail.com/flower1.jpg','image/jpeg','6237',8),(11,'flower2.jpg','upload/mc@gmail.com/flower2.jpg','image/jpeg','3968',8),(12,'flower3.jpg','upload/mc@gmail.com/flower3.jpg','image/jpeg','7581',8),(13,'images.jpg','upload/mc@gmail.com/images.jpg','image/jpeg','4402',8),(14,'nature1.jpg','upload/ac@gmail.com/nature1.jpg','image/jpeg','12099',15),(15,'nature2.jpg','upload/ac@gmail.com/nature2.jpg','image/jpeg','15405',15),(16,'nature3.jpg','upload/ac@gmail.com/nature3.jpg','image/jpeg','13208',15),(17,'nature4.jpg','upload/ac@gmail.com/nature4.jpg','image/jpeg','8722',15);
/*!40000 ALTER TABLE `img` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-10 16:24:46
