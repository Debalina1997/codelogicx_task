<?php

require_once "process.php";
 

$firstname = $lastname = $email = $phoneno = $address = $admin = $password = $confirm_password = "";
$firstname_err = $lastname_err = $email_err = $phoneno_err = $address_err = $admin_err = $password_err = $confirm_password_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    if(empty(trim($_POST["firstname"]))){
        $firstname_err = "Please enter a username.";
    }else{
        $firstname = trim($_POST["firstname"]);
    }

    if(empty(trim($_POST["lastname"]))){
        $lastname_err = "Please enter a lastname.";
    }else{
        $lastname = trim($_POST["lastname"]);
    }
    

    if (empty(trim($_POST["email"]))) {
        $email_err = "Please enter email.";
      } else {
        $sql = "SELECT id FROM user WHERE email = ?";

        $email = trim($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          $email_err = "Invalid email format";
        }
      }

    if(empty(trim($_POST["phoneno"]))){
        $phoneno_err = "Please enter phone no.";     
    } else{
        $phoneno = trim($_POST["phoneno"]);
    }
   
    if(empty(trim($_POST["address"]))){
        $address_err = "Please enter address.";     
    } else{
        $address = trim($_POST["address"]);
    }


    if (!array_key_exists("admin", $_POST)) {
        $admin = 0;
    } else {
        $admin = $_POST["admin"] == "on" ? 1 : 0;
    }
    
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
    

    
    if(empty($firstname_err) && empty($lastname_err) && empty($email_err) && empty($phoneno_err) && empty($address_err) && empty($admin_err) && empty($password_err) && empty($confirm_password_err)){
        
        $password = password_hash($password, PASSWORD_DEFAULT);
        $sql = "INSERT INTO user (firstname, lastname, email, phoneno, address, admin, password) VALUES ('$firstname','$lastname','$email',$phoneno,'$address',$admin,'$password')";
        
        $mysqli->query($sql) or die($mysqli->error);
        
        
         
            /*if($stmt = $mysqli->prepare($sql)){
            $stmt->bind_param("sssssss", $param_firstname, $param_lastname, $param_email, $param_phoneno, $param_address, $param_admin, $param_password);
            
            $param_firstname = $firstname;
            $param_lastname = $lastname;
            $param_email = $email;
            $param_phoneno = $phoneno;
            $param_address = $address;
            $param_admin = $admin;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash?
        
            try {
                $stmt->execute();
                // if($stmt->execute()){
                $path = "photo/".$email;
                if(!is_dir($path)){
                    mkdir($path);
                }
                $path = "upload/".$email;
                if(!is_dir($path)){
                    mkdir($path);
                }
                header("location: log-in.php");
                // } else{
                    // echo "Something went wrong. Please try again later.";
                // }
            } catch(Exception $e) {
                echo($e->getMessage());
            }
            $stmt->close();
        }*/
    }
    $mysqli->close();
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Sign Up</h2>
        <p>Please fill this form to create an account.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($firstname_err)) ? 'has-error' : ''; ?>">
                <label>Firstname</label>
                <input type="text" name="firstname" class="form-control">
                <span class="help-block"><?php echo $firstname_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($lastname_err)) ? 'has-error' : ''; ?>">
                <label>Lastname</label>
                <input type="text" name="lastname" class="form-control">
                <span class="help-block"><?php echo $lastname_err; ?></span>
            </div>  
            <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                <label>Email</label>
                <input type="text" name="email" class="form-control">
                <span class="help-block"><?php echo $email_err; ?></span>
            </div>  
            <div class="form-group <?php echo (!empty($phoneno_err)) ? 'has-error' : ''; ?>">
                <label>Phone no</label>
                <input type="tel" name="phoneno" class="form-control">
                <span class="help-block"><?php echo $phoneno_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($address_err)) ? 'has-error' : ''; ?>">
                <label>Address</label>
                <input type="textarea" name="address" class="form-control">
                <span class="help-block"><?php echo $address_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($admin_err)) ? 'has-error' : ''; ?>">
                <label>Admin</label>
                <input type="checkbox" name="admin" class="form-control">
                <span class="help-block"><?php echo $admin_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control">
                <span class="help-block"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>Already have an account? <a href="log-in.php">Login here</a>.</p>
        </form>
    </div>    
</body>
</html>