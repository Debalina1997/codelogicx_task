<?php

session_start();

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: welcome.php");
    exit;
}

require_once "process.php";

$email = $password = "";
$email_err = $password_err = "";
$admin = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    if(empty(trim($_POST["email"]))){
        $email = "Please enter your email.";
    }else{
        $email = trim($_POST["email"]);
    }

    if(empty(trim($_POST["password"]))){
        $password_err = "please enter password.";
    }else{
        $password = trim($_POST["password"]);
    }

    if(empty($email_err) && empty($password_err)){
        $sql = "SELECT id, email, password,admin FROM user WHERE email = '$email'";
        $result = $mysqli->query($sql) or die($mysqli->error);

        
        // if($stmt = $mysqli->prepare($sql)){
            // $stmt->bind_param("s", $param_email);
            // $param_email = $email;
            

            // if($stmt->execute()){
            //     $stmt->store_result();

            //     if($stmt->num_rows == 1){
            //     $stmt->bind_result($id, $email, $hashed_password);
                if($result->num_rows == 1){
                while($row = $result->fetch_assoc()){
                    $id = $row['id'];
                    $email = $row['email'];
                    $hashed_password = $row['password'];
                    $admin = $row['admin'];
                }
            }
                     if(password_verify($password, $hashed_password)){
                        session_start();

                        $_SESSION["loggedin"] = true;
                        $_SESSION["id"] = $id;
                        $_SESSION["email"] = $email;
                        $_SESSION["admin"] = $admin;
                        
                        header("location: welcome.php");
                    }else{
                        $password_err = "The password you entered was not valid.";
                    }
    }
}
    //     $stmt->close();
    //     }
    // }
    // $mysqli->close();
//}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>

<div class="wrapper">
    <h2>Login</h2>
    <p>Please fill in your credentials to login.</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
            <label>Email</label>
            <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
            <span class="help-block"><?php echo $email_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
            <label>Password</label>
            <input type="password" name="password" class="form-control">
            <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Login">
        </div>
        <p>Don't have an account? <a href="sign-in.php">Sign up now</a>.</p>
    </from>
</div>

</body>
</html>