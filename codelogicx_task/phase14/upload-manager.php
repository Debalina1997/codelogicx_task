<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
  <?php include "navbar.php" ?>

  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</body>
</html>

<?php

session_start();

$mysqli = new mysqli('localhost','root','123456','test') or die($mysqli->connect_error);
$table = 'img';

if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", 
        "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo"]["name"];
        $filetype = $_FILES["photo"]["type"];
        $filesize = $_FILES["photo"]["size"];

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: 
            Please select a valid file format.");

            $maxsize = 4 * 1024 * 1024;
            if($filesize > $maxsize) die("Error: File size is larger than allowed limit.");
            
            if(in_array($filetype, $allowed)){

                
                if(file_exists("upload/" . $filename)){
                    echo $filename . " is already exists.";
                }else{
                    print_r($filename);
                    print_r($_FILES["photo"]["tmp_name"]);

                    print_r($_SESSION["email"]);
                  
                    $img_dir = "upload/" . $_SESSION["email"] . "/" . $filename;
                    $img_type = $_FILES["photo"]["type"];
                    $img_size = $_FILES["photo"]["size"];
                    echo($img_dir);
                    move_uploaded_file($_FILES["photo"]["tmp_name"], $img_dir);

                    $id = $_SESSION['id'];
                    
                    $sql = "INSERT INTO $table (img_name,img_dir,img_type,img_size,id) VALUES ('$filename','$img_dir','$img_type','$img_size',$id)";
                    $mysqli->query($sql) or die($mysqli->error);
                    
                    echo "your file is uploaded successfully";
                }
            }else{
                echo "Error: There is a problem uploading your file. 
                Please try again";
            }
    }else{
        echo "Error: " . $_FILES["photo"]["error"];
    }
}

if($_FILES["photo"]["error"] > 0){
    echo "Error: " . $_FILES["photo"]["error"] . "<br>";
} else{
    echo "File Name: " . $_FILES["photo"]["name"] . "<br>";
    echo "File Type: " . $_FILES["photo"]["type"] . "<br>";
    echo "File Size: " . ($_FILES["photo"]["size"] / 1024) . " KB<br>";
    echo "Stored in: " . $_FILES["photo"]["tmp_name"];
}

?>