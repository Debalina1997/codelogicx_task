<?php

session_start();

require_once "process.php";

$img_dir = "";
$img_name = "";
$img_type = "";
$img_size = "";

if (isset($_SESSION['id'])) {
$id = $_SESSION['id'];

$sql = "SELECT * FROM img WHERE id=$id";
$result = $mysqli->query($sql) or die($mysqli->error);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Display</title>

    <link type="text/css" rel="stylesheet" href="./css/lightslider.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="./js/lightslider.js"></script>


    
</head>

<body>
<?php include "navbar.php" ?>
    <ul id="lightslider">
        <?php while($row = $result->fetch_assoc()) { ?>
            <li class="item-a" data-thumb="http://sachinchoolur.github.io/lightslider/upload/thumb">
                <img src="<?php echo($row['img_dir']) ?>" />
                <h6>Name :<?php echo ($row['img_name']) ?></h6></br>
                <h6>Type :<?php echo ($row['img_type']) ?></h6></br>
                <h6>Size :<?php echo ($row['img_size']) ?></h6>
            </li>
        <?php }
    } ?>
    </ul>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        $('#lightslider').lightSlider({
            // item: 1,
            // autoWidth: false,
            // slideMove: 1, // slidemove will be 1 if loop is true
            // slideMargin: 10,

            // addClass: '',
            // mode: "slide",
            // useCSS: true,
            // cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            // easing: 'linear', //'for jquery animation',////

            // speed: 400, //ms'
            // auto: false,
            // loop: false,
            // slideEndAnimation: true,
            // pause: 2000,

            // keyPress: false,
            // controls: true,
            // prevHtml: '',
            // nextHtml: '',

            // rtl: false,
            // adaptiveHeight: false,

            // vertical: false,
            // verticalHeight: 500,

            // vThumbWidth: 100,

            // thumbItem: 10,
            // pager: true,
            // gallery: false,
            // galleryMargin: 5,
            // thumbMargin: 5,
            // currentPagerPosition: 'middle',

            // enableTouch: true,
            // enableDrag: true,
            // freeMove: true,
            // swipeThreshold: 40,

            // responsive: [],

            // onBeforeStart: function (el) { },
            // onSliderLoad: function (el) { },
            // onBeforeSlide: function (el) { },
            // onAfterSlide: function (el) { },
            // onBeforeNextSlide: function (el) { },
            // onBeforePrevSlide: function (el) { }
        });
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>


</html>