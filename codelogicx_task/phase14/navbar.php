
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="welcome.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="img_upload.php">Upload Image</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="image-gallery.php">View Uploaded Image</a>
        </li>
      
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="admin.php">Admin Page</a>
        </li>
        
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="reset-password.php">Reset Password</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="log-out.php">Logout</a>
        </li>
        </ul>
    </div>
  </div>
</nav>