-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_s`
--

DROP TABLE IF EXISTS `user_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_s` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phoneno` int NOT NULL,
  `address` varchar(255) NOT NULL,
  `admin` int DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_s`
--

LOCK TABLES `user_s` WRITE;
/*!40000 ALTER TABLE `user_s` DISABLE KEYS */;
INSERT INTO `user_s` VALUES (1,'Debalina','Chatterjee','dc@gmail.com',1234567890,'Begharia, Kolkata',NULL,'$2y$10$VQz2LtiVXsTaNyLsKe.hBOXk3W3aTX5zwORIUCmdhWjFqUzB78PSq','2021-02-23 22:49:28',NULL,'http://localhostpublic/uploads/images/users/1.png','2021-02-25 05:31:31'),(2,'Progya','Saha','ps@gmail.com',2147483647,'sodepur,kolkata',NULL,'$2y$10$whbPy7hlUZ9YcWzIf.hn1uLTPA0StYGG6vosEPjr.Fw0YAdYn94F.','2021-02-23 22:55:58',NULL,'http://localhostpublic/uploads/images/users/2.png','2021-02-25 05:28:40'),(3,'Arka','Majumdar','am@gmail.com',1234567888,'Belur, Howrah',NULL,'$2y$10$4LU0L6xcDvmbx48Brw0cJOoW.5aexxLHwbULow5h.IEe/E7.4cBli','2021-02-23 22:57:09',NULL,'http://localhostpublic/uploads/images/users/3.png','2021-02-25 06:28:48'),(12,'Mandira','Chowdhuri','ma@gmail.com',2147483647,'Begharia, Kolkata',NULL,'$2y$10$n5MhSMprmaLpyGapLjSx6OjyuI.yi.lRqpsUZ4peIdqQt65qIVxMG','2021-02-25 06:49:12',NULL,'http://localhostpublic/uploads/images/users/12.png','2021-02-25 06:49:36');
/*!40000 ALTER TABLE `user_s` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-25 19:15:41
