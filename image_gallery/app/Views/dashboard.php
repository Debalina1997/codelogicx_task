<div class="page-header">
    <h1>Hi <?= session()->get('firstname') ?>, Welcome to our site.</h1>
  </div>
  <h6>Name :<?= session()->get('firstname') ?> <?= session()->get('lastname')?></h6></br>
  <h6>Email :<?= session()->get('email') ?></h6></br>
  <h6>Phone no :<?= session()->get('phoneno') ?></h6></br>
  <h6>Address :<?= session()->get('address') ?></h6></br>

 <?= session()->get('profile_image') ?>

  <img src="<?= session()->get('profile_image') ?>" />
  <!-- echo (session()->get('profile_image')) -->

  <form action="/upload_image" method="POST" 
    enctype="multipart/form-data">
    <h2>Upload your picture</h2>
    <label for="fileSelect">Filename:</label>
    <input type="file" name="photo" id="fileSelect">
    <input type="submit" name="submit" id="upload">
    <p><strong>Note:</strong> Only .jpg, .jpeg, .gif, .png formats 
        allowed to a max size of 4 MB.</p>
  </form>