<?php namespace App\Controllers;

use App\Models\UserModel;


class Users extends BaseController
{
	public function index()
	{
		$data = [];
        helper(['form']);

        
        if ($this->request->getMethod() == 'post') {
			//let's do the validation here
			$rules = [
				'email' => 'required|min_length[6]|max_length[50]|valid_email',
				'password' => 'required|min_length[8]|max_length[255]|validateUser[email,password]',
			];

			$errors = [
				'password' => [
			    'validateUser' => 'Email or Password don\'t match'
				]
			];

			if (! $this->validate($rules, $errors)) {
				$data['validation'] = $this->validator;
			}else{
				$model = new UserModel();

				$user = $model->where('email', $this->request->getVar('email'))
											->first();

				$this->setUserSession($user);
				//$session->setFlashdata('success', 'Successful Registration');
				return redirect()->to('dashboard');

			}
		}

        echo view('templates/header', $data);
        echo view('login');
        echo view('templates/footer');
	}

    private function setUserSession($user){
		$data = [
			'id' => $user['id'],
			'firstname' => $user['firstname'],
			'lastname' => $user['lastname'],
			'email' => $user['email'],
            'phoneno' => $user['phoneno'],
            'address' => $user['address'],
			'profile_image' => $user['profile_image'],
			'isLoggedIn' => true,
		];

		session()->set($data);
		return true;
	}

    public function register()
	{
		$data = [];
        helper(['form']);

        if ($this->request->getMethod() == 'post') {
			
			$rules = [
				'firstname' => 'required|min_length[3]|max_length[20]',
				'lastname' => 'required|min_length[3]|max_length[20]',
				'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[user.email]',
                'phoneno' => 'required|min_length[10]',
                'address' => 'required|min_length[6]|max_length[255]',
				'password' => 'required|min_length[8]|max_length[255]',
				'password_confirm' => 'matches[password]',
			];

			if (! $this->validate($rules)) {
				$data['validation'] = $this->validator;
			}else{
                $model = new UserModel();

				$newData = [
					'firstname' => $this->request->getVar('firstname'),
					'lastname' => $this->request->getVar('lastname'),
					'email' => $this->request->getVar('email'),
                    'phoneno' => $this->request->getVar('phoneno'),
                    'address' => $this->request->getVar('address'),
					'admin' => $this->request->getVar('admin'),
                    'password' => $this->request->getVar('password'),
				];
				$model->save($newData);
				$session = session();
				$session->setFlashdata('success', 'Successful Registration');
				return redirect()->to('/');
            }
        }
        echo view('templates/header', $data);
        echo view('register');
        echo view('templates/footer');
	}

	public function profile(){
		
		$data = [];
		helper(['form', 'url']);

		$model = new UserModel();

		// echo(WRITEPATH);

		if ($this->request->getMethod() == 'post') {
			//let's do the validation here
			$rules = [
				'profile_image' => 'required|mime_in[photo,image/jpg,image/jpeg,image/gif,image/png]|max_size[photo,4096]',
			];

			// if (!$this->validate($rules)) {
			// 	$data['validation'] = $this->validator;
			// }else{

				$file = $this->request->getFile('file');
            	$file->move('./public/uploads/images/users', $newName);
				// $file = ('./assets/uploads');

				$data = [
					'profile_image' => WRITEPATH . 'uploads\\' . session()->get('id')
				];
				// echo($data);

				$model->update( ['id' => session()->get('id')], $data);
				session()->setFlashdata('success', 'Successfuly Updated');

				// return redirect()->to('/profile');

			// }
		}

		$data['user'] = $model->where('id', session()->get('id'))->first();
		echo view('templates/header', $data);
		echo view('profile');
		echo view('templates/footer');
	}

	public function store()
    {  

		$data = [];
		helper(['form', 'url']);

		$model = new UserModel();

         
     	$db = \Config\Database::connect();
        $builder = $db->table('user_s');
 
        $validated = $this->validate([
            'photo' => [
                'uploaded[photo]',
                'mime_in[photo,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[photo,4096]',
            ],
        ]);
 
        $msg = 'Please select a valid file';
  
        if ($validated) {
            $file = $this->request->getFile('photo');
            $file->move('assets/uploads/images/users', session()->get('id') . '.png');
		
 
          $data =[
             'profile_image' => base_url() . '/assets/uploads/images/users/' . session()->get('id') . '.png'
		  ];

		$model->update( ['id' => session()->get('id')], $data);
		session()->setFlashdata('success', 'Successfuly Updated');
          $msg = 'File has been uploaded';
        }
 
       return redirect()->to( base_url('/dashboard') )->with('msg', $msg);
 
    }

    public function logout(){
	   session()->destroy();
	   return redirect()->to('/');
    }
}